const express = require('express');
const app = express();
const PORT = 3000;

// GET endpoint - http://localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello!');
});

/**
 * Funktio kertoo kaksi lukua keskenään.
 * @param {number} kerrottava Eka parametri
 * @param {number} kerroin Toka parametri
 * @returns {number} tulos
 */
const multiply = (kerrottava, kerroin) => {
    const tulos = kerrottava * kerroin;
    return tulos;
};

// GET multiply endpoint - http://localhost:3000/multiply?a=3&b=5
app.get('/multiply', (req, res) => {
    try {
        const kerrottava = parseFloat(req.query.a);
        const kerroin = parseFloat(req.query.b);
        if (isNaN(kerrottava) || isNaN(kerroin)) throw new Error('Invalid value');
        console.log({kerrottava, kerroin});
        const tulos = multiply(kerrottava, kerroin)
        res.send(`Tulos antamillasi luvuilla on: ${tulos}`);
    } catch (err) {
        console.error(err);
        res.send(`Voisitko syöttää numeroita seuraavaksi?`);
    }


});

app.listen(PORT, () => console.log(
    `Listening at http://localhost:${PORT}`
));
